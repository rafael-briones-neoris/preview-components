# **Before you start README** #
The ng2BoilerPlate health checker will tell you if the last commit actually works

--->https://bitbucket.org/rafael-briones-neoris/preview-components.git<-----

Hello, before you proceed with any forks or downloads, please make sure your development environment meets the following criteria:

- You have installed the latest version of dotnet core 1.1.0 (or newer)
- You have correctly installed webpack (by using npm install webpack -g)

Second, this repository contains an inner repository (git submodule) so the correct way (and easier) to pull this repository should be:

*git clone https://bitbucket.org/rafael-briones-neoris/preview-components.git *

Finally, this repository will teach you how other projects work, remember that the main file where all dependencies are stored is:

- app.module.ts

In order to make this repository run, you must execute a bash file which bundles a ton of commands:

- wp-start.sh <----THIS FILE is intended to be used in unix systems, please modify or remove the open chrome in case you are using windows or other different system.

### Before FORKing ###
There is one file which tells bluemix where to install the app,

- manifest.yml, sets internal info data such as space of the app and route of the URL
- bitbucket_pipelines, this files tests the code on every commit and by merging to development it sends the code to the development container on bluemix.

*****Change the above files when forking to point to your container*****

if you have any questions, pleas contact me at e-asantosb@neoris.com

-Andres