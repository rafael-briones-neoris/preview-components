YEL='\033[1;33m'
NC='\033[0m'
RED='\033[1;31m'
GREEN='\033[1;32m'
CY='\033[0;36m'
PR='\033[1;35m'

M_PORT=5000
#read options, only -p is valid
while getopts "p:e:" option
do
    case "${option}"
    in
        p) M_PORT=${OPTARG};;
        e) M_ENV=${OPTARG};;
    esac
    
done


dotnet restore
echo "${PR} INSTALLING NPM DEPENDENCIES ..... ${NC}" 
npm install
echo "${GREEN} NPM INSTALL DONE ${NC}"
echo "${RED}#####                     (33%)\r ${NC}"
sleep 1
webpack --config webpack.config.vendor.js
echo "${PR}#############             (66%)\r ${NC}"
sleep 1
export ENV_PROJECT=${M_ENV}&&webpack --config webpack.config.js
export ASPNETCORE_ENVIRONMENT=Development

echo "${GREEN} VENDOR PACKAGES OK ${NC}" 
echo "${GREEN}#######################   (100%)\r ${NC}"
echo '\n'
/usr/bin/open -a "/Applications/Google Chrome.app" 'http://localhost:${M_PORT}'
echo "when the page is load wait few seconds to project start...." 
echo "${GREEN}have ${cy}fun${NC}" 
dotnet run --server.urls=http://localhost:${M_PORT}/