import {Component, Input, Output, EventEmitter} from '@angular/core';
import { CommonModule } from '@angular/common';


@Component({
    selector: 'cmx-chip',
    templateUrl: './cmx-chip.component.html',
    styleUrls: ['./../../../scss/cmx-chip/v1/cmx-chip.component.scss']
})

export class CmxChipComponent{
    @Input() selected: boolean;
    @Input() text:string;
    @Input() index:number;
    @Output() chipRemoved: EventEmitter<number> = new EventEmitter<number>();
    constructor(){
        this.text = "";
    }

    removeChip(){
        console.log(this.index);
        this.chipRemoved.emit(this.index-1);
    }
}