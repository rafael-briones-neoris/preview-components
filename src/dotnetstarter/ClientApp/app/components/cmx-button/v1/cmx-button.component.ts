import { Component, NgModule, ViewEncapsulation, Input, ElementRef, Renderer } from '@angular/core';

@Component({
    selector: 'a[cmx-button-link], a[cmx-btn-link], button[cmx-button-link], button[cmx-btn-link]',
    templateUrl: './cmx-button.component.html',
    styleUrls: [ './../../../scss/cmx-button/v1/cmx-button.component.scss' ],
    host: {
        '[class.cmx-button-link]': 'true'
    }
})
export class ButtonLink{
    constructor(){}
}

@Component({
    selector: 'a[cmx-button], a[cmx-btn], button[cmx-button], button[cmx-btn]',
    templateUrl: './cmx-button.component.html',
    styleUrls: [ './../../../scss/cmx-button/v1/cmx-button.component.scss' ],
    host: {
        '[class.cmx-button]': 'true'
    },
})
export class Button{
    private _color: string;

    @Input()
    get color(): string{
        return this._color;
    }
    set color( value: string ){
        this.updateColor( value );
    }

    constructor( private renderer: Renderer, private elementRef: ElementRef ){}

    private updateColor( newColorValue: string ): void{
        this.setElementColor( this._color, false );
        this.setElementColor( newColorValue, true );
        this._color = newColorValue;
    }

    private setElementColor( color: string, isAdd: boolean ): void{
        if( color !== undefined && color !== '' ){
            this.renderer.setElementClass( this.getHostElement(), color, isAdd );
            let element: any = this.getHostElement();
            if( element.tagName.toLowerCase() === 'a' ){
                this.renderer.setElementClass( element, 'is-anchor', true );
            }
        }
    }

    private getHostElement(): ElementRef{
        return this.elementRef.nativeElement;
    }
}