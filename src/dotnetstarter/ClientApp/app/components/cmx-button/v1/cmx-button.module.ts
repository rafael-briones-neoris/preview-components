import { NgModule } from '@angular/core';
import { ButtonLink, Button } from './cmx-button.component';
import { ButtonLinkStyler, ButtonStyler, ButtonRippleEffect } from './cmx-button.directive';

@NgModule({
    exports: [
        ButtonLink,
        Button,
        ButtonLinkStyler,
        ButtonStyler,
        ButtonRippleEffect
    ],
    declarations: [
        ButtonLink,
        Button,
        ButtonLinkStyler,
        ButtonStyler,
        ButtonRippleEffect
    ]
})
export class CmxButtonModule{}