import { Directive, HostListener } from '@angular/core';

@Directive({
    selector: 'a[cmx-button-link], a[cmx-btn-link], button[cmx-button-link], button[cmx-btn-link]',
    host: {
        '[class.cmx-button-link]': 'true'
    }
})
export class ButtonLinkStyler{
    constructor(){}
}

@Directive({
    selector: 'button[cmx-button], button[cmx-btn]',
    host: {
        '[class.cmx-button]': 'true'
    }
})
export class ButtonStyler{
    constructor(){}
}

@Directive({
    selector: 'button[cmx-button], button[cmx-btn]',
    host: {
        '[class.ripple]': 'true',
        '[style.position]': '"relative"',
        '[style.outline]': '"none"'
    }
})
export class ButtonRippleEffect{
    private rippleColor: string;

    constructor(){
        this.rippleColor = '#ffffff';
    }

    @HostListener( 'click', [ '$event' ] )
    onClick( evt ){
        let button = evt.currentTarget;
        let rippleContainer = button.querySelector( '.cmx-button__ripple-container' );
        let div = document.createElement( 'div' );
        let rect = rippleContainer.getBoundingClientRect();
        let xClickPosition = evt.pageX - rect.left;
        let yClickPosition = evt.pageY - rect.top;
        div.classList.add( '--ripple-effect' );
        div.style.height = rippleContainer.clientHeight;
        div.style.width = rippleContainer.clientHeight;
        div.style.left = ( xClickPosition - ( rippleContainer.clientHeight / 2 ) ) + 'px';
        div.style.top = yClickPosition + 'px';
        rippleContainer.appendChild( div );
        setTimeout( () => {
            rippleContainer.removeChild( rippleContainer.querySelector( '.--ripple-effect' ) )
        }, 2000 );
    }
}