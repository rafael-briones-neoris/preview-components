import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';

@Component({
    selector: 'cmx-nav-head',
    styleUrls: ['../../../scss/cmx-nav-header/v1/cmx-nav-header.component.scss'],
    templateUrl: "./cmx-nav-header.component.html"
})
export class CmxNavHeaderComponent implements OnInit {
    private _labelNotification = "Notifications"
    
    @Input()
    _notifications: any[] = [];

    @Output()
    clickMenuButton: EventEmitter<any> = new EventEmitter<any>();

    _userInitials: string = "CMX";
    _openInput:boolean = false;
    _searchValue:string;

    ngOnInit(): void {

    }

    onClickSearch(){
       if (!this._openInput) {
           this._openInput = true;
       } 
    }

    onClickCancelSearch() {
        this._openInput = false;
    }

    onClickMenuButton(){
        this.clickMenuButton.emit();
    }
}
