import { Component, Directive, OnInit, Input, HostBinding, Output, EventEmitter } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CmxChipComponent } from '../../cmx-chip/v1/cmx-chip.component'

@Component({
    selector:'cmx-chip-input',
    templateUrl: './cmx-chip-input.component.html',
    styleUrls: ['./../../../scss/cmx-chip-input/v1/cmx-chip-input.component.scss']
})

export class CmxChipInputComponent {
    @Input() placeholder: string = 'Add a tag';
    @Input() chips: string[];
    @Input() delimiterCode: string = '188';
    @Input() allowedTagsPattern: RegExp = /.+/;
    @Output() searchClick = new EventEmitter();

    public tagsList: string[];
    public inputValue: string = '';
    public delimiter: number;
    public selectedTag: number;
    constructor(){
        this.chips = [];
        this.tagsList = [];
    }

    ngOnInit(){
        if (this.chips) this.tagsList = this.chips;
        this.onChange(this.tagsList);
        this.delimiter = parseInt(this.delimiterCode);
    }

    inputChanged(event) {
        let key = event.keyCode;
        switch(key) {
        case 8: 
            this.handleBackspace();
            break;
        case 13:
            this.addTags([this.inputValue]);
            event.preventDefault();
            break;
        case this.delimiter:
            this.addTags([this.inputValue]);
            event.preventDefault();
            break;
        default:
            this.resetSelected();
            break;
        }
    }


    inputPaste(event) {
        let clipboardData = event.clipboardData || (event.originalEvent && event.originalEvent.clipboardData);
        let pastedString = clipboardData.getData('text/plain');
        let tags = this.splitString(pastedString);
        let tagsToAdd = tags.filter((tag) => this.isTagValid(tag));
        this.addTags(tagsToAdd);
        setTimeout(() => this.inputValue = '', 1700);
    }

    private splitString(tagString: string) {
        tagString = tagString.trim();
        let tags = tagString.split(String.fromCharCode(this.delimiter));
        return tags.filter((tag) => !!tag);
    }

    public search(click){
        this.searchClick.emit(click)
    }

    private isTagValid(tagString: string) {
        return this.allowedTagsPattern.test(tagString);
    }

    private addTags(tags: string[]) {
        let validTags = tags.filter((tag) => this.isTagValid(tag));
        this.tagsList = this.tagsList.concat(validTags);
        this.resetSelected();
        this.resetInput();
        this.onChange(this.tagsList);
    }

    private removeTag(tagIndexToRemove) {
        this.tagsList.splice(tagIndexToRemove, 1);
        this.resetSelected();
        this.onChange(this.tagsList);
    }

    private handleBackspace() {
        if (!this.inputValue.length && this.tagsList.length) {
            if ((this.selectedTag)) {
                this.removeTag(this.selectedTag-1);
            }
            else {
                this.selectedTag = this.tagsList.length;
            }
        }
    }

    private resetSelected() {
        this.selectedTag = null;
    }

    private resetInput() {
        this.inputValue = '';
    }

    onChange: (value) => any = () => { };
   
}