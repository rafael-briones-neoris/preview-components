//https://long2know.com/2017/01/angular2-dialogservice-exploring-bootstrap-part-2/
import { Injectable } from '@angular/core';
import { NgbModal, NgbModalOptions, NgbActiveModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Component, Input, OnInit, ApplicationRef, ChangeDetectorRef } from '@angular/core';
import { CmxDialogComponent, DialogOptions } from './cmx-dialog.component';

export interface CmxDialogMessage {
    title?: string;
    message: string;
    resultOptions?: DialogOptions;
}

@Injectable()
export class CmxDialogService {

    constructor(private modalService: NgbModal) {

    }

    public openDialog(dialogMessage: CmxDialogMessage, options?: NgbModalOptions): Promise<any> {
        const modalRef = this.modalService.open(CmxDialogComponent, options);
        let ref: CmxDialogComponent = <CmxDialogComponent>modalRef.componentInstance;
        console.log("printing ref:", ref);
        ref.title = dialogMessage.title;
        ref.options = dialogMessage.resultOptions;
        ref.message = dialogMessage.message;
        modalRef.componentInstance.changeRef.markForCheck();
        return modalRef.result;
    }

    public openComponentDialog<T>(component:any, options?: NgbModalOptions){
        const modalRef = this.modalService.open(component, options);
        console.log("printing ref:", modalRef);
        modalRef.componentInstance.changeRef.markForCheck();
        return modalRef.result;
    }



}