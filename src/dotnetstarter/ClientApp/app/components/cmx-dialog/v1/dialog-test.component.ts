import { Component, Input, ChangeDetectorRef, OnInit } from '@angular/core';
import { NgbModalOptions, NgbActiveModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

export interface CmxDialogButton {
    value: any;
    color?: string;
}

export interface DialogOptions {
    [text: string]: CmxDialogButton;
}

@Component({

    styleUrls: ['../../../scss/cmx-dialog/v1/cmx-dialog.component.scss'],
    templateUrl: "./dialog-test.component.html"
})
export class DialogTest implements OnInit {
    @Input()
    title: string;

    @Input()
    message: string;

    options: DialogOptions = {
        "OK": { color: "green", value: true },
        "Cancel": { color: "green", value: false }
    };

    constructor(public activeModal: NgbActiveModal, public changeRef: ChangeDetectorRef) { }

    ngOnInit(): void {

    }

    getKeys() {
        return Object.keys(this.options);
    }
}