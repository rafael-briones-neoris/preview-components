/**
 * npm install --save @types/googlemaps
 * npm install node-js-marker-clusterer --save
 */
import { Component, OnInit, Input, Output, EventEmitter, NgZone, AfterViewInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs';

let MarkerClusterer = require('node-js-marker-clusterer');
export interface IPositionMap {
    latitude: number;
    longitude: number;
}

export interface IGeocodeResponse {
    found: boolean;
    message: string;
    formatted_address: string;
    latitude: number;
    longitude: number;
    results: google.maps.GeocoderResult[]
}

interface IMapDictionary {
    [index: string]: google.maps.Marker;
}

@Component({
    selector: 'cmx-map',
    styleUrls: ['../../../scss/cmx-map/v1/cmx-map.component.scss'],
    templateUrl: "./cmx-map.component.html"

})
export class CmxMapComponent implements OnInit, AfterViewInit {

    @ViewChild('contentMap') containerMap: any;
    @ViewChild('cmxMapId') el: ElementRef;

    @Input() mapOptions: google.maps.MapOptions = MAP_OPTIONS;
    @Input() showLatLng: boolean = true;

    @Output() onMapReady: EventEmitter<CmxMapComponent> = new EventEmitter<CmxMapComponent>();
    @Output() drawingPolygon: EventEmitter<boolean> = new EventEmitter<boolean>();
    @Output() clickOnMarker: EventEmitter<google.maps.Marker> = new EventEmitter<google.maps.Marker>();
    @Output() onDragingMarker: EventEmitter<google.maps.Marker> = new EventEmitter<google.maps.Marker>();

    private _map: google.maps.Map;
    private _drawingManager: google.maps.drawing.DrawingManager;
    private _allShapes: any[] = [];
    private _geocoder: google.maps.Geocoder = new google.maps.Geocoder();
    private _subject = new BehaviorSubject<any>(null);
    private _zoomMap: number = 18;
    private _dictMarker: IMapDictionary = {};
    private _currentLocation = new BehaviorSubject<google.maps.LatLng>(undefined);
    private _showLatLng: boolean = false;
    private _theMarker: google.maps.Marker;

    public set zoomMap(value: number) {
        this._zoomMap = value;
        google.maps.event.trigger(this._map, "bounds_changed");
    }
    constructor(private ref: ChangeDetectorRef) { }

    ngOnInit() {

    }

    /*****************************/
    /*Find div and Initialize map*/
    /*****************************/
    ngAfterViewInit() {
        //this._contentDiv = <HTMLDivElement>this.containerMap.nativeElement;

        this._map = new google.maps.Map(<HTMLDivElement>this.el.nativeElement, this.mapOptions);
        console.log("div-Map:", this.el.nativeElement, " myMap:", this._map, " this.mapOptions:", this.mapOptions);
        //this.centerCurrentLocation();
        this._map.setOptions(this.mapOptions);

        google.maps.event.addDomListener(window, "resize", function () {
            if (this._map) {
                let center = this._map.getCenter();
                google.maps.event.trigger(this._map, "resize");
                this._map.setCenter(center);
            }

        });

        google.maps.event.addListenerOnce(this._map, 'bounds_changed', (event) => {
            console.log("setting zoom at:", this._zoomMap);
            this._map.setZoom(this._zoomMap);
        });

        this.centerCurrentLocation();
        this.onMapReady.emit(this);
    }

    public centerCurrentLocation(...positions: google.maps.LatLng[]) {
        //console.log("entering centerCurrentLocation:", positions);
        this._map.setOptions({ maxZoom: 15 });

        google.maps.event.addListenerOnce(this._map, 'idle', (event) => {
            this._map.setOptions({ minZoom: null, maxZoom: 20 });
        });

        if (positions && positions.length > 0) {
            //console.log("centering with parameters");
            let latlLngBounds = new google.maps.LatLngBounds();
            for (let position of positions) {
                latlLngBounds.extend(position);
            }

            this._map.setCenter(latlLngBounds.getCenter());
            this._map.fitBounds(latlLngBounds);

        } else {
            console.log("doing center of current location");
            if (navigator.geolocation) {
                console.log("doing center of current location");
                navigator.geolocation.getCurrentPosition(position => {
                    var pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                    let latlLngBounds = new google.maps.LatLngBounds();
                    let mypos = new google.maps.LatLng(pos.lat, pos.lng);
                    latlLngBounds.extend(mypos)
                    //google.maps.event.trigger(this._map, "resize");
                    this._map.setCenter(latlLngBounds.getCenter());
                    this._map.fitBounds(latlLngBounds);
                    google.maps.event.trigger(this._map, 'resize');
                });
            }
        }

        /*google.maps.event.addListenerOnce(this._map, 'bounds_changed', (event) => {
            console.log("setting zoom at:", this._zoomMap);
            this._map.setZoom(this._zoomMap);
        });*/

        google.maps.event.trigger(this._map, 'resize');

    }

    public setCenter(position: IPositionMap) {
        if (this._map) {
            let latlLngBounds = new google.maps.LatLngBounds();
            let mypos = new google.maps.LatLng(position.latitude, position.longitude);
            latlLngBounds.extend(mypos)
            this._map.setCenter(latlLngBounds.getCenter());
        }

    }

    public drawPolygon(points: any[]): google.maps.Polygon {
        console.log("drawing points:", points);
        let newShape = new google.maps.Polygon({
            paths: points,
            strokeColor: '#2e8af5',
            strokeOpacity: 0.8,
            strokeWeight: 2.5,
            fillColor: '#139DF2',
            fillOpacity: 0.35
        });

        newShape.setMap(this._map);
        this._allShapes.push(
            {
                type: "polygon",
                overlay: newShape
            });

        return newShape;
    }

    public drawRadial(center: google.maps.LatLng, radius: number): google.maps.Circle {

        let circle = new google.maps.Circle({
            center: center,
            radius: radius,    // 10 miles in metres
            strokeColor: '#2e8af5',
            strokeOpacity: 0.8,
            fillColor: '#139DF2',
            fillOpacity: 0.35
        });

        circle.setMap(this._map);

        this._allShapes.push(
            {
                type: "circle",
                overlay: circle
            });


        return circle;
    }

    public initilizeDrawingManager(options?: google.maps.drawing.DrawingManagerOptions): google.maps.drawing.DrawingManager {
        this._drawingManager = new google.maps.drawing.DrawingManager(options);
        google.maps.event.addListener(this._drawingManager, "overlaycomplete", (event) => {
            this._allShapes.push(event);
            this.drawingPolygon.emit(true);
            console.log("printing event:", event);
        });

        return this._drawingManager;
    }

    public clearAllShapes() {
        for (let shape of this._allShapes) {
            shape.overlay.setMap(null);
        }

        this._allShapes = [];
    }

    public searchPlaceByAddress(address: string) {
        this._geocoder.geocode({ 'address': address },
            (results: google.maps.GeocoderResult[], status: google.maps.GeocoderStatus) => {

                let found = status == google.maps.GeocoderStatus.OK;
                if (found) {
                    console.log(" GeocoderResult address_components:", results[0].address_components);
                }
                let result: IGeocodeResponse = {
                    found: found,
                    message: status.toString(),
                    formatted_address: found ? results[0].formatted_address : undefined,
                    latitude: found ? results[0].geometry.location.lat() : undefined,
                    longitude: found ? results[0].geometry.location.lng() : undefined,
                    results: results,
                }
                this._subject.next(result);
            })
        return this._subject.asObservable();
    }

    public getAllShapes(): any[] {
        return this._allShapes;
    }

    public drawPolyline(points: any[]): google.maps.Polyline {
        //console.log("drawing points:", points);
        let newLine = new google.maps.Polyline({
            path: points,
            strokeColor: '#2e8af5',
            strokeOpacity: 0.8,
            strokeWeight: 2.5,

        });

        newLine.setMap(this._map);
        return newLine;
    }

    public addMarker(markerId: string, marker: google.maps.Marker): void {
        marker.setMap(this._map);
        this._dictMarker[markerId] = marker;

        marker.addListener('click', () => {
            this._theMarker = marker;
            if (this.showLatLng) {
                this._showLatLng = true;
                this.ref.detectChanges();
            }

            this.clickOnMarker.emit(marker);
        });

        marker.addListener("drag", (event) => {
            //console.log("get position drag:", event.latLng);
            if (this.showLatLng){
                this.ref.detectChanges();
            }

            this.onDragingMarker.emit(marker);
        });
    }

    public deleteAllMarkers(): void {
        for (var key in this._dictMarker) {
            let marker: google.maps.Marker = this._dictMarker[key];
            marker.setMap(null);
        }
    }

    public getCurrentLocation(): Observable<google.maps.LatLng> {
        if (navigator.geolocation) {
            console.log("getting current location");
            navigator.geolocation.getCurrentPosition(position => {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                this._currentLocation.next(new google.maps.LatLng(pos.lat, pos.lng));
            });
        } else {
            console.log("returning undefined");
            this._currentLocation.next(undefined);
        }

        return this._currentLocation.asObservable();

    }

}


export const MAP_STYLES: any[] =
    [
        { "stylers": [{ "saturation": -100 }, { "gamma": 0.8 }, { "lightness": 4 }, { "visibility": "on" }] },
        { "featureType": "landscape.natural", "stylers": [{ "visibility": "off" }, { "color": "#5dff00" }, { "gamma": 4.97 }, { "lightness": -5 }, { "saturation": 100 }] },
        { "featureType": "poi", "stylers": [{ "visibility": "off" }] }
    ]

// Google map options
export const MAP_OPTIONS: google.maps.MapOptions = {
    styles: MAP_STYLES,
    mapTypeControl: true,
    mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
        position: google.maps.ControlPosition.TOP_CENTER
    },
    zoomControl: true,
    zoomControlOptions: {
        position: google.maps.ControlPosition.LEFT_CENTER
    },
    scaleControl: true,
    streetViewControl: true,
    streetViewControlOptions: {
        position: google.maps.ControlPosition.LEFT_TOP
    },
};


export const ICON_BASE = "/app/svg/svgmap/";
export const MAP_ICONS = {
    DEFAULT_PIN: {
        icon: `${ICON_BASE}default-marker.svg`
    },
};