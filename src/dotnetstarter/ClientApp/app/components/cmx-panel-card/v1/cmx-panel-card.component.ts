import { Component, Input } from '@angular/core';

@Component({
    selector: 'cmx-panel-card',
    templateUrl: './cmx-panel-card.component.html',
    styleUrls: ['./../../../scss/cmx-panel-card/v1/cmx-panel-card.component.scss']
})

export class CmxPanelCardComponent {
    @Input() title: string;
    @Input() masterHeight: string;
    @Input() minHeight: string;
    @Input() Class: string

    constructor() {
        if (this.masterHeight === undefined) {
            this.masterHeight = "220px";
        }
        if (this.minHeight === undefined) {
            this.minHeight = "0";
        }
        this.Class = "simple-card-border"
    }

}