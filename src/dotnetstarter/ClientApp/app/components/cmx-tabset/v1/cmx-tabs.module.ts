import { NgModule } from '@angular/core';
import { CmxTabSet, CmxTab, CustomTab,  CustomTabPipe, NgTransclude, TabHeading } from './index';
import { CommonModule } from '@angular/common';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        CmxTabSet, CmxTab, CustomTab,  CustomTabPipe, NgTransclude, TabHeading
    ],
    exports: [
        CmxTabSet, CmxTab, CustomTab,  CustomTabPipe, NgTransclude, TabHeading
    ]
})
export class CmxTabsModule{}