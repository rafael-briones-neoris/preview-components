import {
    Component, Input, AfterViewInit, ViewChild, ContentChild,
    ChangeDetectorRef, AfterContentChecked, ElementRef, AfterViewChecked,
    Directive, TemplateRef
} from "@angular/core";

@Component(
    {
        selector: 'custom-tab',
        template: `
    <template tab-heading>
        <ng-content></ng-content>
    </template>
    `
    })
export class CustomTab implements AfterViewInit {

    public headingRef: TemplateRef<any>;

    @ViewChild('container') containerViewChild: ElementRef;
    container: HTMLDivElement;

    ngAfterViewInit() {
        //console.log("the container:", this.containerViewChild);
        if (this.containerViewChild) {
            this.container = <HTMLDivElement>this.containerViewChild.nativeElement;
            this.container.hidden = true;
        }

    }
}


@Component({
    selector: 'tab-sidebar',
    template: `
    <div *ngIf="active" class="content">
    <ng-content></ng-content>
    </div>
  `
})
export class CmxTab implements AfterViewInit, AfterContentChecked, AfterViewChecked {

    @Input() title:string;
    @Input() active = false;
    @Input() disabled = false;
    @Input() showTab = true;
    @Input() idTab: number;
    @ContentChild(CustomTab) customTab: CustomTab;

    public get hasCustomTab(){
        return this._hasCustomTab;
    }

    private _hasCustomTab: boolean = false;

    constructor(private _changeDetectionRef: ChangeDetectorRef) { }

    ngAfterViewInit() {
        this._hasCustomTab = this.customTab != undefined;
        this._changeDetectionRef.detectChanges();
    }

    ngAfterContentChecked() {
       this._hasCustomTab = this.customTab != undefined;
    }

    ngAfterViewChecked() {
        //console.log("this.hasCustomTab:", this.hasCustomTab);
    }

}


@Directive({ selector: '[tab-heading]' })
export class TabHeading {
    constructor(public templateRef: TemplateRef<any>, tab: CustomTab) {
        tab.headingRef = templateRef;
    }
}
