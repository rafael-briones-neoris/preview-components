import {
    Component, OnInit, Input, Output, OnChanges, EventEmitter, ContentChildren,
    AfterContentInit, Pipe, PipeTransform
} from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { CmxTab } from './cmx-tab.component';

@Pipe({
    name: 'customTabPipe'
})
export class CustomTabPipe implements PipeTransform {

    constructor(private _sanitizer: DomSanitizer) {
    }

    transform(tabs:CmxTab[]): CmxTab[] {
        return tabs.filter(tab => {
            return tab.title || tab.hasCustomTab;
        })
    }
}


@Component({
    selector: 'cmx-tabset',
    templateUrl: './cmx-tabset.component.html',
    styleUrls: ['../../scss/cmx-tabset/v1/cmx-tabset.component.scss'],
})
export class CmxTabSet implements AfterContentInit {
    
    @Input() verticalTab:boolean = false;
    @Output() onTabSelected = new EventEmitter();
    @ContentChildren(CmxTab) tabs;

    get tabSelected() {
        return this._tabSelected;
    }

    private _tabSelected: number = 0;

    constructor() {
        this.onTabSelected.emit(this._tabSelected);
    }

    ngAfterContentInit() {
        const tabs = this.tabs.toArray();
        const actives = this.tabs.filter(t => { return t.active });

        if (actives.length > 1) {
            console.error(`Multiple active tabs set 'active'`);
        } else if (!actives.length && tabs.length) {
            tabs[0].active = true;
            this._tabSelected = tabs[0].idTab;
        }
    }

    tabClicked(tab:CmxTab) {
        const tabs = this.tabs.toArray();
        this._tabSelected = tab.idTab;

        tabs.forEach(tab => { 
            tab.active = false;
        });
        
        tab.active = true;
        this.onTabSelected.emit(tab);
    }

    public setTabActive(idTab: number) {
        const tabs = this.tabs.toArray();
        tabs.forEach(tab => {
            tab.active = tab.idTab == idTab;
        });
    }

    
}