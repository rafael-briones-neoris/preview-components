import { Component, Input, Output, EventEmitter } from '@angular/core';

export interface ICmxAlertItem {
    alertId: number;
    alertText: string;
    className?: string;

    alertLink?: {
        linkText: string;

    }

}

@Component({
    selector: 'cmx-alert',
    styleUrls: ['../../../scss/cmx-alert/v1/cmx-alert.component.scss'],
    templateUrl: "./cmx-alert.component.html"
})
export class CmxAlertComponent {

    @Input()
    inputAlert: ICmxAlertItem[];

    @Output()
    clickLink: EventEmitter<ICmxAlertItem> = new EventEmitter<ICmxAlertItem>();

    closeAlert(elem: ICmxAlertItem) {
        let index: number = this.inputAlert.indexOf(elem);
        if (index != -1) {
            this.inputAlert.splice(index, 1);
        }
    }

    onClickLink(alert: ICmxAlertItem) {
        this.clickLink.emit(alert);
    }

}