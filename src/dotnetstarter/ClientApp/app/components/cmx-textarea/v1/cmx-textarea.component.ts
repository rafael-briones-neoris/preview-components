import { Component, Directive, ContentChild, ViewChild, ElementRef, Input, OnInit } from '@angular/core';

@Directive({
    selector: 'label[isFocused]',
})

@Component({
    selector: 'cmx-textarea',
    templateUrl: './cmx-textarea.component.html',
    styleUrls: ['../../scss/cmx-textarea/v1/cmx-textarea.component.scss'],
})
export class MdTextAreaComponent implements OnInit {
    @Input('icon-float')
    private iconFloat: boolean;
    @Input('control')
    private _formControlName: Object;
    @ViewChild('commentsArea')
    private commentsArea: ElementRef;

    constructor(private elementRef: ElementRef) {

    }

    ngOnInit(): void {
        
    }

    
}