import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MdTextAreaComponent } from './index';

@Component({
    selector: 'demo-md-textarea',
    templateUrl: './demo.component.html'
})
export class DemoMdTextAreaComponent implements OnInit{
  
    private _disputeForm: FormGroup;
    private _formBuilder: FormBuilder;    

    constructor( formBuilder: FormBuilder ){
        this._formBuilder = formBuilder;
      
    }

    ngOnInit(){
        this._disputeForm = this._formBuilder.group({
            comments: [ '', Validators.required ],
        });
    }

    processFinished():void{
        alert( "Wizard finished the process" );
    }
}
