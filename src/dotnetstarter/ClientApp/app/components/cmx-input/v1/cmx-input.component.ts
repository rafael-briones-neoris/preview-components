import { Component, Input, ElementRef, OnInit, ViewChild, HostBinding, HostListener, Directive, OnChanges, Renderer} from '@angular/core'
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

@Component ({
    selector: 'cmx-input',
    templateUrl: './cmx-input.component.html',
    styleUrls: ['./../../../scss/cmx-input/v1/cmx-input.component.scss'],
})
export class CmxInputComponent {
    @Input() title:string;
    @Input() errorMessage:string;
    @Input() isWrong:boolean;

    constructor(private _eref:ElementRef, private renderer:Renderer){
        this.isWrong = false;
        this.title = "Title";
        this.errorMessage = "";
    }

    ngOnInit(){       
    }

    ngOnChanges() {
        if (this.isWrong){
            this.renderer.setElementClass(this._eref.nativeElement.children[0].children[1], 'wrong', true);
        } else {
            this.renderer.setElementClass(this._eref.nativeElement.children[0].children[1], 'wrong', false);
        }
    }
}