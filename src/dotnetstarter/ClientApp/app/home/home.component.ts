import { Component } from '@angular/core';
import { CmxDialogService, CmxDialogMessage } from '../components/cmx-dialog/v1/cmx-dialog.service';
import { CmxDialogComponent } from '../components/cmx-dialog/v1/cmx-dialog.component';
import { ICmxAlertItem } from '../components/cmx-alert/v1/cmx-alert.component';
import { CmxMapComponent, MAP_ICONS } from '../components/cmx-map/v1/cmx-map.component';
import { DialogTest } from '../components/cmx-dialog/v1/dialog-test.component';

@Component({
	selector: 'home',
	styleUrls: ['./home.component.css'],
	templateUrl: './home.component.html'
})
export class HomeComponent {
	hi: boolean;

	inputAlert: ICmxAlertItem[] = [
		{
			alertId: 1,
			alertText: "alert 1...",

		}
	];

	ialert: number = 1;
	public tags: string[];

	constructor(private dialogService: CmxDialogService) { }

	showModal() {
		let message: CmxDialogMessage = {
			title: "Discard Changes?",
			message: "Are you sure you want to discard your changes?",
			resultOptions: { "Cancel": { value: false }, "Ok": { value: true, color: "green" } }
		}
		let callback: Promise<any> = this.dialogService.openDialog(message);
		callback.then(
			(result: any) => {
				console.log("result:", result);
			}
		)
			.catch(
			(reason: any) => {
				console.warn("reason:", reason);
			}
			)
	}

	showComponentModal() {
		let callback: Promise<any> = this.dialogService.openComponentDialog(DialogTest);
		callback.then(
			(result: any) => {
				console.log("result:", result);
			}
		)
			.catch(
			(reason: any) => {
				console.warn("reason:", reason);
			}
			)
	}

	hola() {
		this.hi = !this.hi;

	}

	addAlert() {
		this.ialert++;
		this.inputAlert.push(
			{
				alertId: this.ialert,
				alertText: `alert ${this.ialert}...`
			}
		)
	}

	addGreen() {
		this.ialert++;
		this.inputAlert.push(
			{
				alertId: this.ialert,
				alertText: `alert ${this.ialert}...`,
				className: "alert-green"
			}
		)
	}

	addAlertLink() {
		this.ialert++;
		this.inputAlert.push(
			{
				alertId: this.ialert,
				alertText: `alert ${this.ialert}...`,
				className: "alert-green",
				alertLink: {
					linkText: "click this link"
				}
			}
		)
	}

	alertLinkResult(alert: ICmxAlertItem) {
		console.log("clicked alert link:", alert);
	}


	private onMapReady(mapComponent: CmxMapComponent) {

		mapComponent.getCurrentLocation().subscribe((position: google.maps.LatLng) => {
			console.log("current location:", position);
			let myMarker = new google.maps.Marker({
				position: position,
				icon: MAP_ICONS.DEFAULT_PIN.icon,
				draggable: true
			});

			mapComponent.addMarker("mymarker", myMarker);
		});
	}

	clickMenuButton() {
		console.log("you click menu button from header");
	}
}
