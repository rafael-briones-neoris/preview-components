import { NgModule } from '@angular/core'
import { RouterModule } from '@angular/router';
import { rootRouterConfig } from './app.routes';
import { AppComponent } from './app.component';
import { GithubService } from './github/shared/github.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { FlexLayoutModule } from '@angular/flex-layout';

import { HomeComponent } from './home/home.component';
// CMX Components
import { CmxButtonModule } from './components/cmx-button/v1/';
import { CmxInputComponent } from './components/cmx-input/v1/cmx-input.component';
import { CmxFrameComponent } from './components/cmx-frame/v1/cmx-frame.component';
import { CmxDialogComponent } from './components/cmx-dialog/v1/cmx-dialog.component';
import { DialogTest } from './components/cmx-dialog/v1/dialog-test.component';
import { CmxDialogService } from './components/cmx-dialog/v1/cmx-dialog.service';
import { CmxAlertComponent } from './components/cmx-alert/v1/cmx-alert.component';
import { CmxChipComponent } from './components/cmx-chip/v1/cmx-chip.component';
import { CmxChipInputComponent } from './components/cmx-chip-input/v1/cmx-chip-input.component';
import { CmxPanelCardComponent } from './components/cmx-panel-card/v1/cmx-panel-card.component';
import { CmxMapComponent } from './components/cmx-map/v1/cmx-map.component';
import { CmxNavHeaderComponent } from './components/cmx-nav-header/v1/cmx-nav-header.component';
import { CmxNavHeaderModule } from '@cemex/cmx-nav-header-v1/dist';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CmxFrameComponent,
    CmxInputComponent,
    CmxDialogComponent,
    CmxAlertComponent,
    CmxChipComponent,
    CmxChipInputComponent,
    CmxPanelCardComponent,
    CmxMapComponent,
    CmxNavHeaderComponent,
    DialogTest
  ],
  imports: [
    BrowserModule,
    FormsModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    HttpModule,
    CmxButtonModule,
    CommonModule,
    RouterModule.forRoot(rootRouterConfig, { useHash: true }),
    NgbModule.forRoot(),
    CmxNavHeaderModule
  ],
  providers: [
    GithubService,
    CmxDialogService
  ],
  bootstrap: [AppComponent],
  entryComponents: [CmxDialogComponent, DialogTest]
})
export class AppModule {

}
